(defproject prisoners "0.3.0"
  :description "A Prisoner's Dilemma simulation"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [incanter "1.5.4"]
                 [org.clojure/math.numeric-tower "0.0.4"] ]
  :main prisoners.core)
