(ns prisoners.test.strategy
  (:require [prisoners.strategy :as stg]
            [clojure.test :refer :all]))

(defn- last-play [strategy]
  (last (:plays strategy)))

(defn- create-strategy-with-opponent-plays [name opponent-plays]
  (assoc (stg/strategy-constructor name) :opponent opponent-plays))

(defn- play-once [name opponent-plays]
  (-> (create-strategy-with-opponent-plays name opponent-plays)
      stg/play
      :plays
      last))

(deftest sucker-strategy
  (testing "Sucker always cooperates."
    (is (every? #{:coop} [(play-once :sucker [])
                          (play-once :sucker [:coop])
                          (play-once :sucker [:defect])]))))

(deftest grudger-strategy
  (testing "Grudger cooperates until the opponent defects."
    (is (every? #{:coop} [(play-once :grudger [])
                          (play-once :grudger [:coop])]))
    (is (every? #{:defect} [(play-once :grudger [:defect])
                            (play-once :grudger [:coop :defect])]))))

(deftest tit-for-tat-strategy
  (testing "Tit-for-tat plays what the opponent played last and cooperates on first move."
    (is (every? #{:coop} [(play-once :tit-for-tat [])
                          (play-once :tit-for-tat [:coop])]))
    (is (= :defect
           (play-once :tit-for-tat [:defect])))))

(deftest cheat-strategy
  (testing "Cheat always defects"
    (is (every? #{:defect} [(play-once :cheat [])
                            (play-once :cheat [:coop])
                            (play-once :cheat [:defect])]))))
