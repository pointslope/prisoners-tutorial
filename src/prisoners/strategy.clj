(ns prisoners.strategy)

(defprotocol Playable
  "A playable abstraction has an internal play function
   that implements the mechanics of making a move"
  (-play [playable move]))


(defprotocol Scorable
  "A scorable abstraction reports its scores and total"
  (scores [strategy])

  (total [strategy]))

;;  A generic strategy implementation. The strategy algorithm is driven
;;  by the multimethod dispatched based on the strategy name.
;;
;;  name: A keyword identifier for the strategy.
;;
;;  points: A sequence of points where each item represents the points won in
;;          that round.
;;
;;  plays: A sequence of plays where each item represents the play made by the
;;         strategy in that round. Valid plays are `:coop` and `defect`.
;;
;;  opponent: A sequence of plays where each item represents the play made by
;;            the opponent in that round.
(defrecord Strategy [name points plays opponent]
  Scorable

  (scores [this]
    (->> this :points (reductions +)))

  (total [this]
    (reduce + (:points this)))

  Playable

  (-play [this move]
    (update-in this [:plays] conj move)))

(defn strategy-constructor
  "Initializes a data structure for the named strategy."
  [named]
  (->Strategy named [] [] []))

;; ### Strategy Implementations

(defmulti play
  "The `play` multimethod dispatches on the data structure's `:name` attribute."
  :name)

;; The `:random` strategy is a baseline for comparison.
;; Any given move is randomly chosen to be cooperate or
;; defect.
(defmethod play :random [this]
  (-play this (rand-nth [:defect :coop])))

;; The `:sucker` strategy always cooperates
(defmethod play :sucker [this]
  (-play this :coop))

;; The `:grudger` strategy will cooperate until its
;; opponent defects. Thereafter, it will always defect.
;; It is less *forgiving* than `:tit-for-tat`.
;;
;; IMPLEMENT ME!
(defmethod play :grudger [this]
  this)

;; The `:tit-for-tat` strategy plays whatever
;; its opponent played last and cooperates
;; if given the first move.
;;
;; IMPLEMENT ME!
(defmethod play :tit-for-tat [this]
  this)

;; The `:cheat` strategy always defects
;;
;; IMPLEMENT ME!
(defmethod play :cheat [this]
  this)
