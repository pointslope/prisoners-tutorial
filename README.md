# Prisoners

This is an iterative
[Prisoner's Dilemma](http://en.wikipedia.org/wiki/Prisoner's_dilemma)
simulation.  Payoffs are based on Richard Dawkins' description in
[The Selfish Gene](http://www.amazon.com/gp/product/B000SEHIG2/ref=as_li_ss_tl?ie=UTF8&tag=xmlblog-20&linkCode=as2&camp=217145&creative=399373&creativeASIN=B000SEHIG2).

Visualizations are implemented using the
[Incanter library](http://incanter.org/).

## Project Objective

The objective of this project is to introduce developers to some of Clojure's
key abstraction tools: Protocols, Records and Multimethods. Developers
will have the opportunity to implement a number of strategy algorithms
and play them against each other.

## Strategy Algorithms

### Random

The `Random` strategy chooses a move at random.

### Sucker

The `Sucker` strategy always cooperates.

### Cheat

The `Cheat` strategy always defects.

### Grudger

The `Grudger` strategy will cooperate until its opponent
defects. Thereafter, it will always defect.

## Tit-for-tat

The `Tit-for-tat` strategy plays whatever its opponent played last and
cooperates if given the first move.

## Project Structure

The project is composed of a game engine, strategy implementations and
a game visualization utility.

- game.clj contains the game engine implementation.
- strategy.clj contains the strategy implementation. This will be where
  developers will implement the strategy algorithms.
- core.clj contains the app Main function and visualization utility.

## Project Tests

Game play tests are located in `test/prisoners/test/core.clj` and strategy
tests are located in `test/prisoners/test/strategy.clj`. The tests will fail
until the strategies are implemented. They can be run from the command
line via ```lein test```.

It is recommended to use the tests as a guide for the strategy algorithm
implementations.

## Getting Started

- Run ```lein deps``` to download the dependencies
- Run ```lein repl``` to experiment

    > (use 'prisoners.core)
    
    > (graph (game/play-rounds 30 :random :sucker))

- Run ```lein run 30 random sucker``` to play 30 rounds with the
  _random_ and _sucker_ strategies.

## Contributors

Special thanks to these fine folks who contributed to this project:

- [Alan Malloy](https://github.com/amalloy)

## License

Copyright (C) 2011 Christian Romney

Distributed under the Eclipse Public License, the same as Clojure.
